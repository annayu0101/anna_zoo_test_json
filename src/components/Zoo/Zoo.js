import React, { PropTypes } from 'react'
import {Button, FormGroup, FormControl, Table} from 'react-bootstrap'
import Icon from 'react-fa'
import bootbox from 'bootbox'


import CreateModal from './CreateModal.js'
import DetailModal from './DetailModal.js'

export default class Zoo extends React.Component {
  constructor(props) {
    super(props)
  }

  onClickDelete(index) {
    let animal = this.props.animals[index]
    bootbox.dialog({
      title: "刪除資料集",
      message: '<div style="width:100%; white-space:nowrap; overflow:hidden; text-overflow:ellipsis">確定刪除資料集 -'+ animal.name+'?</div>',
      buttons: {
        cancel: {
          label: "取消",
          className: "btn-danger"
        },
        success: {
          label: "確定",
          className: "btn-info",
          callback: (confirm) => {
            if (confirm) {
              let animals = this.props.animals
              animals.splice(index, 1)
              this.props.deleteAnimals(animals)
            }
          }
        }
      }
    })
  }

  onClickAnimalDetail(index){
    let animal = this.props.animals[index]
    let saveDetailModalState = () => {
      this.props.saveDetailModalState(this, true)
    }
    this.props.getAnimalDataSet(animal, saveDetailModalState)
  }

  onClickAnimalEdit(index){
    let animal = this.props.animals[index]
    animal['type'] = 'edit'
    animal['index'] = index
    let saveCreateModalState = () => {
      this.props.saveCreateModalState(this, true)
    }
    this.props.getAnimalDataSet(animal, saveCreateModalState)
  }

  render() {
    let animals = this.props.animals.map((animal, index) => {
      return(
        <tr key={index}>
          <td style={{wordBreak: "break-all", width:'10%'}}><a style={{cursor:'pointer'}} onClick={this.onClickAnimalDetail.bind(this, index)}>{animal.name}</a></td>
          <td style={{wordBreak: "break-all", width:'20%'}}>{animal.description}</td>
          <td>{animal.update}</td>
          <td>
            <Button bsStyle="info" bsSize="xsmall" onClick={this.onClickAnimalEdit.bind(this, index)}><Icon name="pencil"/></Button>
            &nbsp;
            <Button bsStyle="danger" bsSize="xsmall" onClick={this.onClickDelete.bind(this, index)}><Icon name="trash-o"/></Button>
          </td>
        </tr>
      )
    })

    let show
    if(this.props.animals.length){
      show = (
        <tr>
          <td>名稱</td>
          <td>描述</td>
          <td>更新時間</td>
          <td>執行動作</td>
        </tr>
      )
    }else{
      show = (
        <tr>
          <td>目前尚未有資料</td>
        </tr>
      )
    }

    return (
      <div className="container">
        <h1>Zoo</h1>
        <br/>
        <Button bsStyle="info" className="pull-right" onClick={this.props.saveCreateModalState.bind(this, true)}>新增一隻蠢動物</Button>
        <br/><br/><br/>
        <Table striped bordered>
          <thead>
            {show}
          </thead>
          <tbody>
            {animals}
          </tbody>
        </Table>
        <CreateModal {...this.props}/>
        <DetailModal {...this.props}/>
      </div>
    )
  }
}
