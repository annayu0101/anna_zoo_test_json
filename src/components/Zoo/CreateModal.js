import React, { PropTypes } from 'react'
import {Button, Modal, Table, FormGroup, FormControl, Radio} from 'react-bootstrap'
import moment from 'moment'

export default class CreateModal extends React.Component {
  constructor(props) {
    super(props)
  }

  handleAnimalTmpChange(key, e) {
    let animal = this.props.animal
    animal[key] = e.target.value
    this.props.handleAnimalTmpChange(animal)
  }

  setAnimal(){
    let animal = this.props.animal
    animal["validationErrors"] = {}

    if (this.props.animal.name === undefined || this.props.animal.name === "") {
      animal["validationErrors"]["name"] = 'error'
    }
    if (this.props.animal.description === undefined || this.props.animal.description === "") {
      animal["validationErrors"]["description"] = 'error'
    }
    if (this.props.animal.sex === undefined || this.props.animal.sex === "") {
      animal["validationErrors"]["sex"] = 'error'
    }

    if (!Object.keys(animal["validationErrors"]).length) {

      animal['update'] = moment().format('YYYY-MM-DD HH:mm:ss')
      this.props.handleAnimalTmpChange(animal)
      let saveCreateModalState = () => {
        this.props.saveCreateModalState(false)
      }
      if (animal.type === 'edit'){
        this.props.updateAnimal(this.props.animal, this.props.animals, saveCreateModalState)
      }else{
        this.props.setAnimal(this.props.animal, saveCreateModalState)
      }
      this.props.handleAnimalTmpChange({validationErrors:{}})

    }else{
      this.props.handleAnimalTmpChange(animal)
    }
  }

  closeCreateModal(){
    this.props.saveCreateModalState(false)
    this.props.handleAnimalTmpChange({validationErrors:{}})
  }

  render() {
    return (
      <Modal animation={false} show={this.props.createModalState} onHide={this.closeCreateModal.bind(this)}>
        <Modal.Header closeButton>
          <Modal.Title>新增動物</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table bordered>
            <tbody>
              <tr>
                <th style={{width:"20%"}}>ID</th>
                <th>
                  <FormGroup style={{marginBottom:0}}
                    validationState= {this.props.animal.validationErrors.name}>
                    <FormControl type="text"
                      placeholder="ID"
                      onChange={this.handleAnimalTmpChange.bind(this, 'name')}
                      value={this.props.animal.name}/>
                    <span style={{color:'#a94442'}}>{this.props.animal.validationErrors.name === 'error' ? "請填入姓名" : ""}</span>
                  </FormGroup>
                </th>
              </tr>
              <tr>
                <th style={{width:"20%"}}>描述</th>
                <th>
                  <FormGroup  style={{marginBottom:0}}
                    validationState= {this.props.animal.validationErrors.description}>
                    <FormControl type="text"
                      placeholder="描述"
                      onChange={this.handleAnimalTmpChange.bind(this, 'description')}
                      value={this.props.animal.description}/>
                    <span style={{color:'#a94442'}}>{this.props.animal.validationErrors.description === 'error' ? "請填入描述" : ""}</span>
                  </FormGroup>
                </th>
              </tr>
              <tr>
                <th style={{width:"20%"}}>性別</th>
                <th>
                  <FormGroup onChange={this.handleAnimalTmpChange.bind(this, 'sex')}
                    validationState= {this.props.animal.validationErrors.sex}>
                    <Radio name="sex" inline value="male" checked={this.props.animal.sex === "male"}>男</Radio>
                    &nbsp;&nbsp;
                    <Radio name="sex" inline value="female" checked={this.props.animal.sex === "female"}>女</Radio>
                    <span style={{color:'#a94442'}}>{this.props.animal.validationErrors.sex === 'error' ? "請選擇性別" : ""}</span>
                  </FormGroup>
                </th>
              </tr>
            </tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle="info" onClick={this.setAnimal.bind(this)}>確定</Button>
          <Button bsStyle="danger" onClick={this.closeCreateModal.bind(this)}>取消</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}
