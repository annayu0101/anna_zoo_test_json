import React from 'react'
import {hashHistory,  Link} from 'react-router'
import {Navbar, Nav, NavItem} from 'react-bootstrap'

import '../public/bootstrap/css/bootstrap.min.css';
import '../public/bootstrap/js/bootstrap.min.js';

export default class App extends React.Component {
  constructor(props) {
    super(props)
  }

  handleSelect(selectedKey){
    hashHistory.push(selectedKey)
  }

  render() {
    return (
        <div>
          <Navbar inverse className="app-navbar" onSelect={this.handleSelect.bind(this)}>
            <Navbar.Header>
              <Navbar.Brand>
                <Link to={"/"}>Home</Link>
              </Navbar.Brand>
            </Navbar.Header>
            <Nav>
              <NavItem eventKey="/home">Anna</NavItem>
            </Nav>
            <Nav>
              <NavItem eventKey="/zoo">Zoo</NavItem>
            </Nav>
          </Navbar>
          <div className="container">
            {this.props.children}
          </div>
        </div>
    )
  }
}
