export const createModalState = (state = false, action) => {
  switch (action.type) {
    case 'SAVE_CREATE_MODAL_STATE':
      return action.state
    default:
      return state
  }
}

export const detailModalState = (state = false, action) => {
  switch (action.type) {
    case 'SAVE_DETAIL_MODAL_STATE':
      return action.state
    default:
      return state
  }
}

export const animal = (state = {validationErrors:{}}, action) => {
  switch (action.type) {
    case 'HANDLE_ANIMAL_TMP_CHANGE':
      let animal = Object.assign({}, action.animal)
      return animal
    default:
      return state
  }
}

export const animals = (state = [], action) => {
  switch (action.type) {
    case 'SET_ANIMAL_LIST':
      return [...state, action.animal]

    case 'UPDATE_ANIMALS':
      let animals = action.animals
      let animal = action.animal
      animals[animal.index] = animal
      return animals

    case 'DELETE_DATA':
      let animalss = JSON.parse(JSON.stringify(action.animals));
      return animalss

    default:
      return state
  }
}
